import React from "react";
import date from "date-and-time";

import "../css/roundbrowser.css";

import config from "../config/config.json";

class RoundBrowser extends React.Component {
	constructor(props) {
		super(props);

		this.roundDatas = props.roundDatas;
		this.datePattern = date.compile("DD.MM.YYYY@H:mm:ss");
	}

	render() {
		const roundRows = [];
		// Copy the array for sorting later
		const sortedRoundData = this.roundDatas.slice();

		const presentation = (
			<div className="appPresentation">
				<img className="appLogo" src={require("../rsc/uscm.png")} alt="Colonial Theatre Logo"/>
				<span className="appTitleFluff">Welcome to the</span>
				<span className="appTitle">Colonial Theatre</span>
			</div>
		);

		// No rounds to display
		if(!config.availability.roundsAvailable || !this.roundDatas.length) {
			return (
				<div className="roundBrowser">
					{presentation}

					<h1>No rounds are available, sorry :(</h1>
				</div>
			);
		}

		// Actually sort here
		sortedRoundData.sort((a,b) => {
			const aEnd = date.parse(a.data["end"], this.datePattern);
			const bEnd = date.parse(b.data["end"], this.datePattern);

			// Greater date means it happened more recently, so place it first
			if(aEnd > bEnd) {
				return -1;
			}
			return 1;
		});

		let iterations = 1;
		for(const round in sortedRoundData) {
			// Don't show any more rounds
			if(iterations > config.availability.roundsAvailable) {
				break;
			}

			const roundInfo = sortedRoundData[round];

			// Make the round length presentable
			let lengthHours = String(Math.round(roundInfo.data["length"] / 3600)) + "h";
			let lengthMinutes = String(Math.round((roundInfo.data["length"] / 60) % 60)) + "min";

			const startDate = date.parse(roundInfo.data["start"], this.datePattern);
			const paddedDate = startDate.getDate() < 10 ? "0" + String(startDate.getDate()) : startDate.getDate();
			const paddedMonth = (startDate.getMonth()+1) < 10 ? "0" + String((startDate.getMonth()+1)) : (startDate.getMonth()+1);

			const row = (
				<tr key={roundInfo.data["rid"]} className="roundListing"
					onClick={() => {this.props.onRoundSelected(roundInfo.data['rid'])}}
				>
					<td>{roundInfo.data["name"]}</td>
					<td>{roundInfo.data["map"]}</td>
					<td>{roundInfo.data["outcome"]}</td>
					<td>{lengthHours + " " + lengthMinutes}</td>
					<td>{paddedDate + "." + paddedMonth + "." + startDate.getFullYear()}</td>
				</tr>
			);

			roundRows.push(row);
			iterations += 1;
		}

		return (
			<div className="roundBrowser">
				{presentation}

				<table className="roundTable">
					<thead>
						<tr className="roundHeaderRow">
							<th>Name</th>
							<th>Map</th>
							<th>Outcome</th>
							<th>Length</th>
							<th>Date</th>
						</tr>
					</thead>

					<tbody>
						{roundRows.map((row) => row)}
					</tbody>
				</table>
			</div>
		);
	}
}

export default RoundBrowser;
