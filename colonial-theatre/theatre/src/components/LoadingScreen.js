import React from "react";

import '../css/loading.css';

function LoadingScreen(props) {
	return (
		<div className="loadingContainer">
			<div className="loadingLogo">
				<img className="loadingImage ungaA" src={require("../rsc/unga.png")} alt=""/>
				<img className="loadingImage ungaB" src={require("../rsc/unga.png")} alt=""/>
			</div>
			<h1 className="loadingText">Loading</h1>
		</div>
	);
}

export default LoadingScreen;
