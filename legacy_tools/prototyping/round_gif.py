import json
from PIL import Image, ImageDraw, ImageFont
from round import Round
from history import History, Snapshot

SQUAD_COLORS = {
	"Alpha": (216, 49, 72),
	"Bravo": (216, 182, 58),
	"Charlie": (209, 71, 216),
	"Delta": (73, 88, 216)
}

if __name__ == "__main__":
	data = None
	with open("game0.json", "r") as file:
		data = json.load(file)

	meta = data["history_meta"]
	general_info = data["game_info"]
	player_infos = data["player_infos"]
	player_history = data["player_history"]

	rnd = Round(meta["snapshots"], meta["start"], meta["end"], general_info["gamemode"], general_info["map"], general_info["outcome"])
	# Construct each player's history
	for ref in player_infos:
		player_history = History(rnd, ref, data["player_history"][ref])
		info = player_infos[ref]

		player_history.set_key(info["key"])
		player_history.set_name(info["name"])
		player_history.set_faction(info["faction"])
		player_history.set_role(info["role"])
		player_history.set_squad(info["squad"])

		# Add the history to the round
		rnd.add_history(player_history)

	frames = []
	snapshots_to_animate = 200
	for i in range(snapshots_to_animate):
		print(f"{(i/snapshots_to_animate)*100}%")

		image = Image.open("almayer.png")
		width, height = image.size
		pixels = image.load()

		for player in rnd.participants():
			snap = player.get_snapshot(i)
			if snap is None:
				continue
			# only show the almayer
			if snap.get_z() != 3:
				continue

			color = (100, 215, 145)
			squad = player.get_squad()
			if squad != "none":
				color = SQUAD_COLORS[squad]

			for ox in range(16):
				for oy in range(16):
					# BYOND has origin at the lower left corner of the map
					pixels[(snap.get_x()-1)*16 + ox, (height - (snap.get_y())*16) + oy] = color

		# dont kill my disk space :)
		smaller = image.resize((width // 4, height // 4), Image.NEAREST)
		frames.append(smaller)

	print("saving gif...")
	frames[0].save("round.gif", format="GIF", append_images=frames, save_all=True, duration=3, fps=60, loop=0, optimize=True)
