class Round:
	'''
		A round object represents a full game round and all its tracked participants
	'''

	# All participant histories of the game
	histories = []

	# How many snapshots the round consisted of
	snapshots = 0

	# When the game started
	start = 0
	# When it ended
	end = 0

	# What gamemode was played
	gamemode = "unknown"

	# The map that was played on
	round_map = "unknown"

	# The outcome of the round
	outcome = "unknown"

	def __init__(self, snapshots, start, end, gamemode, round_map, outcome):
		self.snapshots = snapshots
		self.start = start
		self.end = end
		self.gamemode = gamemode
		self.round_map = round_map
		self.outcome = outcome
		
	def add_history(self, history):
		self.histories.append(history)

	def get_snapshots(self, snapshot_no):
		for history in self.histories:
			if len(history.snapshots) <= snapshot_no:
				continue
			yield history.get_snapshot(snapshot_no)

	def participants(self):
		'''
			Provides an iterable over all participants in the round
		'''

		for participant in self.histories:
			yield participant
